$(document).ready(function(){
    //vertical menu hover
    $(".cate li").hover(function() {
        $(this).find('.cate-level-2').stop().fadeIn();
    }, function() {
        $(this).find('.cate-level-2').stop().delay(1000).hide(0);
    });

    $(".platform-cates li").hover(function() {
        $(this).find('.menu-blog-lv2').stop().fadeIn();
    }, function() {
        $(this).find('.menu-blog-lv2').stop().delay(1000).hide(0);
    });

    //toggle click search box
    $("#search-toggle").on('click', function(e){
        e.preventDefault();
        $('.frm-search-box').toggleClass('uk-hidden');
    });
    $(document).on('mouseup', function(e){
        var sbox = $('.frm-search-box');
        if (!sbox.is(e.target) && sbox.has(e.target).length === 0){
            sbox.addClass('uk-hidden');
        }
    });

    //toggle advanced search
    $("#toggle-search").on('click', function(e){
        e.preventDefault();
        var inner = $("#advanced-search-box").is(':visible') ? '<img src="img/search-tru-icon.png" alt="">' : '<img src="img/search-plus-icon.png" alt="">'
        $(this).html(inner);
    });

    $(".btn-chitiet").on('click', function(e){
        e.preventDefault();
        var inner = $("#inner-vdown-content").is(':visible') ? 'Thu gọn' : 'Chi tiết';
        $(this).text(inner);
    });

    //handle file upload profile
    $("#file-upload-button").change(function () {
        var fileName = $(this).val().replace('C:\\fakepath\\', '');
        $("#file-upload-filename").html(fileName);
    });

    //handle modal login-register
    var modal_login = UIkit.modal("#modal-login");
    var modal_register = UIkit.modal("#modal-register");
    var modal_foprgot = UIkit.modal("#modal-forgot-pass");
    $("#cl-register").bind('click', function(event) {
        event.preventDefault();
        modal_register.show();
    });

    $("#cl-login").bind('click', function(event) {
        event.preventDefault();
        modal_login.show();
    });

    $("#cl-forgot").bind('click', function(event) {
        event.preventDefault();
        modal_foprgot.show();
    });    

    //handle tab select default image
    (function(){
        $(".tab-selectimg li").bind('click', function(event) {
            var tabId = $(this).attr('data-tab');

            $(".tab-selectimg li").removeClass('curent');
            $(".tab-pane").removeClass('curent');

            $(this).addClass('curent');
            $("#"+ tabId).addClass('curent');
        });
    })(jQuery);

    //handle scroll to top
    (function(){
        $(window).scroll(function(){
            if($(window).scrollTop() > 0){
                $(".btn-scoll-top").fadeIn("slow");
            } else {
                $(".btn-scoll-top").fadeOut("slow");
            }
        });
        $(".btn-scoll-top").bind("click", function(e){
            e.preventDefault();
            $("html,body").animate({scrollTop: 0},"slow");
        });
    })(jQuery);

    // handle menu offcanvas
    $(function(){
        $(window).on('load resize', function() {
            var $body = $("body");
            var $overlay = $("#overlay");
            var $btn_show_menu = $("#show-menu-offcanvas");
            var $offcanvas_menu = $("#offcanvas-menu");
            var $btn_hide_menu = $("#offcanvas-menu-hide");

            var width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
            var height = (this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height;

            $btn_show_menu.on('click', function(event) {
                event.preventDefault();
                $body.addClass('offcanvas-page').css({
                    'width': width + 'px',
                    'height': height + 'px'
                });
                $overlay.addClass('show');
                $offcanvas_menu.addClass('offcanvas-menu-show');
                
            });

            $btn_hide_menu.on('click', function(event) {
                event.preventDefault();
                $body.removeClass('offcanvas-page').attr('style', '');
                $overlay.removeClass('show');
                $offcanvas_menu.removeClass('offcanvas-menu-show');
            });
        });    
    });

    $(window).on('orientationchange', function() {
        var width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        var height = (this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height;
        var $body = $("body");
        if ($body.hasClass('offcanvas-page')) {
            $body.css({
                'width': width + 'px',
                'height': height + 'px'
            });
        };
    });

    $("#offcanvas-menu").mCustomScrollbar({
        theme: "minimal-dark"
    });

});